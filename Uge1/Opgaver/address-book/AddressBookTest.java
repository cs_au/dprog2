public class AddressBookTest
{
	
	public static void main(String[] args)
	{
		checkArgs(args);
	}
	
	/*
	 * Run the interface of either GUI or CLI, depending on which
	 * parameter is typed first. As soon as an interface is started,
	 * break from the loop.
	 * If no valid arguments are found, mock the user.
	 */ 
	public static void checkArgs(String[] args)
	{
		for (String s : args)
		{
			if (s.toLowerCase().equals("-gui"))
			{
				startGUI();
				return;
			}
			else if (s.toLowerCase().equals("-cli"))
			{
				startCLI();
				return;
			}
		}
		
		System.out.println("No valid arguments, fool!");
	}
	
	public static void startGUI()
	{
		AddressBookDemoGUI gui = new AddressBookDemoGUI();
		System.out.println("Starting GUI, for the win...");
		gui.showInterface();
		System.out.println("GUI started, rock on!");
	}
	
	public static void startCLI()
	{
		AddressBookDemoText cli = new AddressBookDemoText();
		System.out.println("Starting CLI, for the loss...");
		cli.showInterface();
	}
}