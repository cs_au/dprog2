Exceptions and Files

DISPOSITION:
- Introduktion
	- Defensive programming
	- Hvad er exceptions?
- Checked vs. unchecked
	- Throwable hierakiet
	- Regler for checked exceptions (throws, try-catch)
- Exception handling
	- throw new Exception()
	- Unchecked exceptions beh�ver ingen h�ndtering
	- Checked exceptions: public void lol() "throws" Exception
	- Checked exceptions: try{ ... } catch(Exception e){ ... }
	- finally
- Error recovery/avoidance
	- Recovery ved at l�gge try-catch i et loop og "retry"
	- Avoidance ved at checke for null og ugyldige return-values
- File handling
	- Hvorfor er exceptions relevante for filer?
	- Tekstfiler: FileReader, BufferedReader, FileWriter
	- Andre filer: InputStream, OutputStream
	- Serialization



Exceptions er en smart m�de at h�ndtere programfejl p�. Brug af exceptions betyder,
at man kan undg� situationer hvor return-values for metodekald skal valideres p� alle 
mulige m�der, fx" if (value == -1)" eller "if (value < 0)". Klienter tvinges samtidig
til at tage handling hvis der sker fejl.

- Defensive programming
	- G� ud fra, at programmet bliver afviklet i et fjendtligt milj� med egentlige
	  fjender, men ogs� inkompetente brugere.
	- Check gyldighed af inputs, return-values, etc.

- Hvis man ikke har exceptions?
	- Return-values som fungerer som "fejl"-values (fx -1, null)
		- Kr�ver mange if-else-statements, som g�r, at det bliver mere uoverskueligt
		  og kompliceret at separere fejlh�ndteringskode fra andet kode.
		- Ingen m�de at tvinge "klienter" til at checke om returv�rdier er gyldige
		  eller ej, s� der er en uheldig risiko for, at programmet dermed forts�tter
		  afvikling med ugyldige v�rdier, som �del�gger andre resultater - eller at 
		  programmet crasher.
		  
- Throwable hierakiet
	- Unchecked exceptions (Throwable <- Exception <- RuntimeException)
		- Compileren kr�ver ikke, at unchecked exceptions h�ndteres, men man kan stadig
		  h�ndtere dem (try-catch-blok) hvis man vil, p� samme m�de som med checked
		  exceptions.
		- Bruges til situationer hvor et program kan fejle som resultat af inkompetente
		  programm�rer p� den ene eller anden m�de, fx hvis et metode bliver bedt om
		  at hente data fra plads "-1" i et array. Dette er en logisk fejl, som sikkert
		  kunne undg�s vha. nogle checks.
			- Fx NullPointerException, IllegalArgumentException
	- Checked exceptions (Throwable <- Exception)
		- Compileren kr�ver h�ndtering af checked exceptions (try-catch-blok)
		- Bruges til situationer hvor et program kan fejle som resultat af noget, som
		  programm�ren ikke har kontrol over - fx hvis man fors�ger at skrive en fil
		  til en disk som er fuld.
			- Fx IOException

- Exception handling
	- For at kaste exceptions bruges "throw".
		- Fx "throw new IOException()"
		- Metoder, som kaster checked exceptions skal i metode-signaturen indeholde
		  "throws" (bem�rk s'et) efterfulgt af hvilke typer exceptions som kastes.
			- Fx "public void addFile(String filename) throws IOException".
		- Metoder som kaster unchecked exceptions beh�ver ingen "throws" i signaturen,
		  men der er ingen regler imod det.
		- Metoder kan "kaste exceptions videre" (propagate an exception). For checked
		  exceptions skal metoderne have "throws" i deres signatur, ogs� selvom det
		  ikke er metoden selv som kaster en exception (det kunne v�re et kald til en
		  anden metode, som netop kaster exceptions). For unchecked exceptions kan
		  "throws" udelades.
	- Exceptions h�ndteres ved at "beskytte" kode, som kan kaste exceptions i en try-
	  blok efterfulgt af en catch-blok, som "fanger" den type exceptions der kan blive
	  kastet fra try-blokken.
		- 	try
			{
				writeToFile(filename);
			}
			catch (IOException e)
			{
				System.out.println("Unable to save to " + filename);
			}
		- Catch efterf�lges af en parentes indeholdende den type af exception som der
		  �nskes h�ndteret i den efterf�lgende blok. Man kan have flere catch-blokke
		  efter en try-blok, s� man kan specificere hvilken type fejl programmet har
		  lavet.
			- "catch (Exception e)" vil fange ALLE exceptions, da alle exceptions er
			  subtyper til Exception (polymorfi).
			- Det kr�ves, at en catch-blok f�lgende en anden catch-blok ikke fanger
			  exceptions som er supertyper til de exceptions som fanges i ovenst�ende
			  catch-blokke. Dvs. "catch (Exception e)" m� alts� ikke komme f�r
			  "catch (IOException)".
	- try-catch-blokke kan f�lges af en valgfri "finally"-blok.
		- finally-blokken udf�res uanset om der kastes en exception eller ej. Den er
		  ofte udeladt, men kan bruges til "oprydningsarbejde".
		- Den er ikke redundant da den ogs� k�res selvom der fx er et return-statement
		  i try-blokken. Desuden k�res den ogs� selvom en kastet exception ikke bliver
		  fanget af catch-blokken.

- Error recovery and avoidance
	- Man kan indkapsle try-catch-blokken i et loop, hvor man laver en slags "retry"
	  ved at modificere elementer i catch-blokken.
		- Fx hvis man fors�ger at l�se en fil som ikke eksisterer, kan man i catch-
		  blokken �ndre p� filnavnet eller pr�ve at kigge i n�rliggende mapper,
		  inkrementere en "MAX_ATTEMPTS" integer, og s� lade try-blokken k�re igen.
	- Man kan fors�ge at undg� exceptions og errors ved at lave relevante checks p�
	  return-values, f�r man blindt benytter dem.
		- Typisk check "if (value != null) ..."

- File handling
	- Exceptions er meget relevante for filh�ndtering, da der er mange tilf�lde hvor
	  programm�ren ingen kontrol har over det milj� filerne befinder sig i.
		- Typiske filh�ndteringsproblemer:
			- �bne ikke-eksisterende filer.
			- �bne filer som er i brug.
			- Skrive filer til en fuld harddisk.
	- java.io bibliotek til filh�ndtering
		- To typer filer
			- Tekstfiler: filer best�ende af l�selige bogstaver. H�ndteres med
			  readers/writers: FileReader, BufferedReader, FileWriter
			- Binary filer: filer best�ende af byte-sekvenser. H�ndteres med streams:
			  FileInputStream, FileOutputStream
		- Filen �bnes, data l�ses/skrives, filen lukkes.
			- Til skrivning af tekstfiler benyttes FileWriter.
			 	FileWriter writer = new FileWriter(filename);
				writer.write(text);
				writer.close();
			- Til l�sning af tekstfiler wrappes FileReader objekter ofte i
			  BufferedReaders, da FileReader ikke kan l�se hele linjer ad gangen.
				FileReader fr = new FileReader();
				BufferedReader reader = new BufferedReader(fr);
				reader.readLine();
				reader.close();
		- Omringes af try-catch-blok, hvori bl.a. IOExceptions fanges.
		- Scanner
			- Kan l�se input fra terminalen
			- Kan ogs� l�se input fra tekstfiler og finde betydningsfulde dele af
			  inputtet vha. dens "parsing" metoder, fx "nextInt()".
	- Serialization
		- Hvis en klasse implementerer interfacet Serializable kan et objekt af
		  klassen gemmes som en bin�r fil i �n write operation (ObjectOutputStream)
		- Objektet kan ligeledes indl�ses igen (ObjectInputStream)
