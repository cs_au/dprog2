import java.util.*;
import java.io.*;

/**
 * Counts all the words in the file specified
 * in the parameter of the constructor.
 * Can be used in a thread.
 */
public class WordCount implements Runnable
{
	private final int DELAY = 10;
	
	private int wordCount = 0;
	private String filename;
	private Scanner scan;
	private Queue<Integer> queue;
	
	/**
	 * @param filename Any text-based file
	 * @precondition filename must be a valid and
	 * existing file readable by the Scanner class
	 */
	public WordCount(String filename, Queue<Integer> queue)
	{
		this.queue = queue;
		
		try
		{
			this.filename = filename;
			scan = new Scanner(new File(filename));
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}
	
	/**
	 * Runs through all the words in the file of the
	 * scanner object, and prints out the wordcount to
	 * the console when finished.
	 */
	public void run()
	{
		try
		{
			while (scan.hasNext())
			{
				scan.next();
				wordCount++;
				Thread.sleep(DELAY);
			}
		}
		catch(InterruptedException e)
		{
		}
		
		// Add the wordcount for this specific file to the queue.
		queue.add(wordCount);
		
		System.out.println(filename + ": " + wordCount + " words");
	}
}