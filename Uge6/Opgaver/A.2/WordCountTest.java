import java.util.concurrent.*;
import java.io.*;

/**
 * Counts all the words in all files specified
 * in the arguments list.
 */
public class WordCountTest
{
	private static BlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>();
	private static int totalWordCount = 0;
	
	public static void main(String[] args)
	{
		/* If no arguments are provided, insult user and use
		 * the 3 default test-files */
		if (args.length == 0)
		{
			System.out.println("Lazy bum! Using 3 default test-files...");
			args = new String[3];
				args[0] = "test1.txt";
				args[1] = "test2.htm";
				args[2] = "WordCount.java";
		}
		
		/* Create a new thread for each filename in the
		 * arguments array. The existence-check is mandatory,
		 * because of WordCount's precondition.
		 * Prints error message if file does not exist. */
		for (String s : args)
		{
			try
			{
				if (new File(s).exists())
				{
					Runnable r = new WordCount(s, queue);
					Thread t = new Thread(r);
					t.start();
				}
				else
				{
					throw new IOException(s + " does not exist!");
				}
			}
			catch(IOException e)
			{
				System.out.println(e);
			}
		}
		
		/* Start the "queue-listener" thread to start adding
		 * up the wordcounts. */
		gallop(args);	
	}
	
	/**
	 * Computes the total wordcount by calling the take()-method
	 * on the queue for each element in the args-array.
	 * The take()-method removes the head of the queue, if there
	 * is anything to remove, otherwise it waits. This is the
	 * reason for wrapping the method in a thread, so that the
	 * program can continue running.
	 */
	private static void gallop(final String[] args)
	{
		Thread hest = new Thread(new Runnable()
		{
			public void run()
			{
				// Call take() once for each file.
				for (String s : args)
				{
					try
					{
						totalWordCount += queue.take();
					}
					catch (InterruptedException e)
					{
						System.out.println("ABORT! ABORT!");
					}
				}
				
				// Print the total wordcount when done.
				System.out.println("Total wordcount for all files: "
					+ totalWordCount + " words");
			}
		});
		
		// Start thread.
		hest.start();
	}
}