import java.util.*;

public class MultiSetTest00 {
	public static void main(String[] args) {
		MultiSet<String> a = new MultiSet<String>();
		MultiSet<String> b = new MultiSet<String>();
		
		a.add("Foo");
		a.add("Bar");
		a.add("Foo");
		System.out.println("a:" + a); // test toString

		b.add("Bar");
		b.add("Foo");
		b.add("Bar");
		b.add("Foo");
		System.out.println("b:" + b);

		assert !a.equals(b); // test equals
		assert b.remove("Bar"); // test remove
		System.out.println("b.size() = "+b.size()); int i = 0;
		assert a.equals(b);
		for(String s : a) { // test iterator
			System.out.println("a = " + a + ", i("+s+")"+", b= "+b);
			assert b.remove(s):"["+s+"] is not removed from b" + b;i++;
		}
		System.out.println("a = " + a + ", b= "+b);
		assert b.size() == 0:""+i+" a.size() = "+a.size();

		List<String> c1 = Arrays.asList("PING!","","PONG�","PANE$", "toto", "toto", "");
		Set<String> c2 = new HashSet<String>(c1);
		
		System.out.println("");
		System.out.println("ArrayList ("+c1+"): " + new MultiSet(c1));
		System.out.println("Set ("+c2+"): " + new MultiSet(c2));
		
		
		try {
			assert false;
			System.out.println("Please enable assertions!");
		}
		catch(AssertionError e) {
			System.out.println("Success!");
		}
	}
}
