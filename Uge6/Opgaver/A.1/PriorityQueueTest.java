import java.util.*;
public class PriorityQueueTest
{
  public static void main(String[] args)
  {
    PriorityQueue<Integer> q = new PriorityQueue<Integer>();
    q.add(new Integer(4));
    q.add(new Integer(7));
    q.add(new Integer(9));
    q.add(new Integer(3));
    q.add(new Integer(3));
    q.add(new Integer(7));
    q.add(new Integer(0));
    q.add(new Integer(1));
    q.add(new Integer(0));
    System.out.println(q);
    System.out.println(q.remove());
    System.out.println(q.remove());
    System.out.println(q.remove());
    System.out.println(q);

    Set baseSet = new HashSet<Integer>(q);
    System.out.println(baseSet);
    
  }
}
