dProg2 - noter

Afleveringen fra sidste uge:
	sammenligne 2 tal:
		teori: bruge meget lille tal (det g�r vi)
		praksis: bruge relativt lille tal
			eg. (relativt til x): x*Math.POW(2,-50)

Frameworks
	inversion of control:
		(Hookpoints eU Metoder/Klasser m. metoder)

	Applet:
		Framework styrer hvorn�r metoder k�rer (applet)
			(paint, stop, ...)
		Browser siger start, osv.: udenfor dine h�nder
	Hovedgrund til at bruge frameworks:
		spare tid
		(risikabelt at implementere kode, der er implementeret i forvejen (skal sikre fejlfri igen))
		JQuery er framework til JavaSript - Det er s� kompliceret at frameworks g�r det nemmere

RandomAcces - hurtig, n�r man tager fat i et givent objekt
	<Marker interface> (giver metadata)
	--> Det betyder at random acces er hurtig
	Det man g�r er at skrive @RandomAcces lige f�r metodekaldet
		eg.:
			/**
			 * @return fast random accessed E 
			 */
			@RandomAcces
			public E hest(){ ... }
ligesom serializable
	indeholder ingen metoder, men indikerer bestemt tankebane ved annotation

9.1 - runnable er ikke tilgivet en thread (/eller 2)
9.3 - der er altid min 2 tr�de
	eg.:
		(min) 1 til java.Swing frameworked {listeners, closeoperations}
		1 til mainthread
	eg.: JFrame �bner sin egen tr�d
9.5 - v�lger 0 eller 2, fordi de er klar (s. 367 - Thread ?Safety?)
	"waiting" venter p� glob kald 'Resurcer er ledige'
		S� har den mulighed for at bes�tte resurcerne
		deadlock er et concurrency problem

Racecondition (s.376)
	2 threads manipulerer p� samme data p� een gang
	--> undg� vha. - telefonboksmetoden, dvs. luk d�ren n�r du ringer
		Synchronized: l�ser objectet p� kaldet
	--> kig gerne i java.util.concurrency.[*/Lock] -> metodevis

9.7 - Sleep er tidsvis
	await venter p� resurcer - (en anden kalder notifyAll() )

Thread er deprecated (de sidste 10 �r)
	- brug ikke stop() !!!

primitive typer kan ikke bruges som generics
