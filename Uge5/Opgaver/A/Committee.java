import java.util.*;

public class Committee
{
	private String name;
	private Manager chairman;
	private Set<Employee> members;

	/**
	 * Creates a new Committee object with a given name
	 * and chairman (Manager) object.
	 */
	public Committee(String name, Manager chairman)
	{
		this.name = name;
		this.chairman = chairman;
		members = new HashSet<Employee>();
	}
	
	/**
	 * Adds an Employee to the members-set.
	 */
	public void add(Employee e)
	{
		members.add(e);
	}
	
	/**
	 * Removes an Employee from the members-set.
	 */
	public void remove(Employee e)
	{
		members.remove(e);
	}
	
	/**
	 * Returns a multi-line string with the name of the
	 * Committee object as well as the values of the
	 * toString()-methods in the Manager and Employee
	 * objects in the Committee.
	 */
	public String toString()
	{
		String nl = System.getProperty("line.separator");
		
		String result = "Committee: " + name;
		result += nl + "Chairman: " + chairman.toString();
		
		for (Employee e : members)
		{
			result += nl + "- " + e.toString() + "	" + e.hashCode();
		}
		
		return result;
	}
	
	/**
	 * Returns the sum of all the hashcodes of all the
	 * Employee and Manager objects in the Committee
	 * object, multiplied by prime number for added
	 * uniqueness.
	 */
	public int hashCode()
	{
		int result = chairman.hashCode();
		
		for (Employee e : members)
		{
			result += e.hashCode();
		}
		
		return 19 * result;
	}
	
	/**
	 * Follows the same basic equality testing scheme
	 * as the one of Employee.
	 */
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null) return false;
		if (getClass() != o.getClass()) return false;
		
		Committee c = (Committee) o;
		return name.equals(c.name)
			&& chairman.equals(c.chairman)
			&& members.equals(c.members);
	}
}