/* **************************************************** *
	Manager-Employee-Committee
	
	The purpose of this driver-class is to test the
	implementations of the methods
		- toString()
		- hashCode()
		- equals()
	in the Employee, Manager and Committee classes.
	
	The driver is set up to simply create a couple of
	objects of each class and run the three methods
	along with a few other tests to check if all of
	the implementations are coded properly - which, of
	course, they are! ;)
	
	- List vs. Set in Committee-class
	For this specific application, the Set interface
	works great; it doesn't care about the order in
	which its elements are added when it comes to
	equality testing, and it doesn't allow duplicate
	entries. The latter is especially useful here,
	since it utilizes the overriden equals-methods.
	
 * **************************************************** */
public class Driver
{
	public static void main(String[] args)
	{	
		Employee e1 = new Employee("e1");
		Employee e2 = new Employee("e2");
		Employee e3 = new Employee("e1");	// Same as e1
		e1.setSalary(100);
		e2.setSalary(100);
		e3.setSalary(100);					// Same as e1
		
		Manager m1 = new Manager("m1");
		Manager m2 = new Manager("m2");
		Manager m3 = new Manager("m1");		// Same as m1
		m1.setSalary(100);
		m1.setBonus(20);
		m2.setSalary(200);
		m2.setBonus(50);
		m3.setSalary(100);					// Same as m1
		m3.setBonus(20);					// Same as m1
		
		System.out.println("********** toString():");
		System.out.println("e1: " + e1);
		System.out.println("e2: " + e2);
		System.out.println("e3: " + e3);
		System.out.println("m1: " + m1);
		System.out.println("m2: " + m2);
		System.out.println("m3: " + m3);
		System.out.println();
		
		System.out.println("********** hashCode():");
		System.out.println("e1.hashCode(): " + e1.hashCode());
		System.out.println("e2.hashCode(): " + e2.hashCode());
		System.out.println("e3.hashCode(): " + e3.hashCode());
		System.out.println("m1.hashCode(): " + m1.hashCode());
		System.out.println("m2.hashCode(): " + m2.hashCode());
		System.out.println("m3.hashCode(): " + m3.hashCode());
		System.out.println();
		
		System.out.println("********** equals() and ==:");
		System.out.println("e1.equals(e1): " + e1.equals(e1)
			+ "	e1 == e1: " + (e1 == e1));
		System.out.println("e1.equals(e2): " + e1.equals(e2)
			+ "	e1 == e2: " + (e1 == e2));
		System.out.println("e1.equals(e3): " + e1.equals(e3)
			+ "	e1 == e3: " + (e1 == e3));
		System.out.println("e3.equals(e1): " + e3.equals(e1)
			+ "	e3 == e1: " + (e3 == e1));
		System.out.println("m1.equals(m1): " + m1.equals(m1)
			+ "	m1 == m1: " + (m1 == m1));
		System.out.println("m1.equals(m2): " + m1.equals(m2)
			+ "	m1 == m2: " + (m1 == m2));
		System.out.println("m1.equals(m3): " + m1.equals(m3)
			+ "	m1 == m3: " + (m1 == m3));
		System.out.println("m1.equals(e1): " + m1.equals(e1)
			+ "	m1 == e1: " + (m1 == e1));
		System.out.println();
		
		
		
		  /*****************************/
 		 /* 	 Committee tests	  */
		/*****************************/
		
		Employee e4 = new Employee("Carsten");
		Employee e5 = new Employee("Birgit");
		Employee e6 = new Employee("Anton");
		e4.setSalary(600);
		e5.setSalary(326);
		e6.setSalary(197);
		
		Committee c1 = new Committee("Hest", m1);
		c1.add(e1);
		c1.add(e2);
		c1.add(e3);
		c1.add(e4);
		
		// New name, chairman and more members
		Committee c2 = new Committee("Ko", m2);
		c2.add(e1);
		c2.add(e2);
		c2.add(e3);
		c2.add(e4);
		c2.add(e5);
		c2.add(e6);
		
		// Same as c1, but different order
		Committee c3 = new Committee("Hest", m1);
		c3.add(e4);
		c3.add(e3);
		c3.add(e1);
		c3.add(e2);
		
		System.out.println("********** Committee toString():");
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3);
		System.out.println();
		
		System.out.println("********** Committee hashCode():");
		System.out.println("c1.hashCode(): " + c1.hashCode());
		System.out.println("c2.hashCode(): " + c2.hashCode());
		System.out.println("c3.hashCode(): " + c3.hashCode());
		System.out.println();
		
		System.out.println("********** Committee equals() and ==:");
		System.out.println("c1.equals(c1): " + c1.equals(c1)
			+ "	c1 == c1: " + (c1 == c1));
		System.out.println("c1.equals(c2): " + c1.equals(c2)
			+ "	c1 == c2: " + (c1 == c2));
		System.out.println("c1.equals(c3): " + c1.equals(c3)
			+ "	c1 == c3: " + (c1 == c3));
		System.out.println("c3.equals(c1): " + c3.equals(c1)
			+ "	c3 == c1: " + (c3 == c1));
		System.out.println();
	}
}