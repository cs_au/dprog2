import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Lommeregner
{	
	private static int n, x;
	//private static JFrame frame;
	public static void main(String[] args)
	{
		//n = args[3]
		showGUI();
	}	
	
	private static void showGUI()
	{		
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//JPanel panel = new JPanel();
		final JLabel resLabel = new JLabel("Result: ");
		
		JButton updateRes = new JButton("Hest");
		updateRes.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					resLabel.setText("Result: "+ Exp(x, n));
					frame.pack();
				}
			});
		
		
		final JLabel nLabel = new JLabel("n slider ("+n+")");
		final JSlider nSlider = new JSlider(0, 31,n);
		nSlider.addChangeListener(new 
			ChangeListener(){
				public void stateChanged(ChangeEvent e)
				{
					n = nSlider.getValue();
					nLabel.setText("n slider ("+n+")");
				}
			});
		
		
		final JLabel xLabel = new JLabel("x slider ("+x+")");
		final JSlider xSlider = new JSlider(0, 20,x);
		xSlider.addChangeListener(new 
			ChangeListener(){
				public void stateChanged(ChangeEvent e)
				{
					x = xSlider.getValue();
					xLabel.setText("x slider ("+x+")");
				}
			});		
		
		//panel.add(slider);
		//panel.add()
		
		//frame.add(panel);
		frame.setLayout(new FlowLayout());
		frame.add(resLabel);
		frame.add(updateRes);
		frame.add(nLabel);
		frame.add(nSlider);
		frame.add(xLabel);
		frame.add(xSlider);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	public static int Exp(int x, int n)
	{	
		if (n == 0)
			return 1;
		else if (n == 1)
			return x;
		else if (n % 2 == 0 && n != 2)
			return Exp(Exp(x,n/2),2);
		else
			return x * Exp(x,n-1);
	}
	
	public static int binomial(int x)
	{
		return x;
	}
}