import java.text.*;
/**
	use Split with regex and use decode.decimal.double
	to get numbers.
	should maybe use Numberformat.parseObject(String source, ParsePosition pos)
	or java.text --> Class DecimalFormat()
		public Number parse(String text, ParsePosition pos)		
*/
public class Computor
{
	private String input;
	private int start;
	
	private DecimalFormat fDecForm = new DecimalFormat();
	
	/**
	 * Constructor
	 */
	public Computor(String anInput)
	{
		start = 0;
		input = anInput;
	}
	

	public String peekToken()
	{		
		ParsePosition pos = new ParsePosition(start);
		
		Number c = fDecForm.parse(input, pos);
		if (c != null)
			return Double.toString(c.doubleValue());
		else 
		{
			return (start >= input.length())?
						null:
						Character.toString(input.charAt(start));
		}
	}
	

	public String nextToken()
	{
		String res;
		
		ParsePosition pos = new ParsePosition(start);
		
		Number c = fDecForm.parse(input, pos);
		if (c != null)
			res = Double.toString(c.doubleValue());
		else 
		{
			int n = pos.getIndex();
			pos.setIndex(n+1);
			res = (n >= input.length())?
						null:
						Character.toString(input.charAt(n));
		}
		start = pos.getIndex();
		//System.out.println(res);
		return res;
	}
}





