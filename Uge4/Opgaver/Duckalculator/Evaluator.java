// import java.text.*; //my edit
/**
 * A class that can compute the value of an arithmetic expression.
 * Supports 
 *  * numbers (doubles)
 *  * operators ( + - * / )
 *  * paratheses ( ( ) )
 */
public class Evaluator
{
   /**
      Constructs an evaluator.
      @param anExpression a string containing the expression
      to be evaluated.
   */
   public Evaluator(String anExpression) {
      tokenizer = new Computor(anExpression);
   }

   /**
      Evaluates the expression.
      @return the value of the expression.
   */
   public double getExpressionValue() {
      double value = getTermValue();
System.out.println("\nTerm1: "+value);
      while ("+".equals(tokenizer.peekToken())
         ||  "-".equals(tokenizer.peekToken()))
	  {
         String operator = tokenizer.nextToken();
         double value2 = getTermValue();
System.out.println("\nTerm2: "+value2);
         if ("+".equals(operator)) value = value + value2;
            else value = value - value2;
      }
      return value;
   }

   /**
      Evaluates the next term found in the expression.
      @return the value of the term.
   */
   public double getTermValue() {
      double value = getFactorValue();
System.out.println("\nFactor1: "+value);
      while ("*".equals(tokenizer.peekToken())
	 ||  "/".equals(tokenizer.peekToken())) {
	 String operator = tokenizer.nextToken();
         double value2 = getFactorValue();
System.out.println("\nFactor2: "+value2);
         if ("*".equals(operator)) value = value * value2;
         else value = value / value2;
      }
      return value;
   }

   /**
      Evaluates the next factor found in the expression.
      @return the value of the factor.
   */
   public double getFactorValue() {
      double value;
      if ("(".equals(tokenizer.peekToken())) {
         tokenizer.nextToken();
         value = getExpressionValue();
         tokenizer.nextToken(); // read ")"
      }
      else {
		String tmp = tokenizer.nextToken();
		//System.out.println(tmp);
		value = Double.parseDouble(tmp); // hack div10 -why?
		// value = tokenizer.nextToken(true);
		
	  }
System.out.println("\nfinal: "+value);
      return value;
   }

   private Computor tokenizer;
}
