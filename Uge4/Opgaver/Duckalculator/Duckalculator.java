import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.text.*;


public class Duckalculator
{
	private static final String NWL = System.getProperty("line.separator");
	
	private static JTextArea history = new JTextArea("please enter an expression");
	private static JTextPane textArea = new JTextPane();
	
	public static void main(String[] args)
	{
		ShowGUI();
	}
	
	private static boolean shouldEmptyResult;
	
	private static void ShowGUI()
	{
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JScrollPane historyPane = new 
			JScrollPane(
						   history,
						   ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
						   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
					   );
		
		JPanel numPanel = new JPanel();
		// keypad layout 4*4
		numPanel.setLayout(new GridLayout(4,4));
		String keyLabels =  "123+"
						  + "456-"
						  + "789*"
						  + "0/,=" ;   
		
		for (int i=0; i<keyLabels.length(); i++)
		{
			final String label = keyLabels.substring(i, i + 1);
			JButton bu = new JButton(label);
			numPanel.add(bu);
			if (i < keyLabels.length()-1) {
				bu.addActionListener(new
					ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							textArea.setText(textArea.getText() + label);
							textArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
						}
					});
			}
			else {
				bu.addActionListener(new
					ActionListener()
					{						
						public void actionPerformed(ActionEvent e)
						{
							String tmp = compute();
							history.append(NWL+textArea.getText());
							textArea.setText(tmp);
							scrolldown();
							textArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
						}
					});
			}
		}
		
		textArea.setBorder(new 
				CompoundBorder(
					new EmptyBorder(2,2,2,2), 
					new EtchedBorder()));		

		frame.setLayout(new BorderLayout());
		frame.add(historyPane, BorderLayout.CENTER);		
		frame.add(textArea, BorderLayout.SOUTH);
		JPanel mix = new JPanel();
			mix.setLayout(new BoxLayout(mix, BoxLayout.Y_AXIS));
			mix.add(textArea);
			mix.add(numPanel);
		frame.add(mix, BorderLayout.SOUTH);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * Workaround for Bug ID: 	4201999
	 * @href: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4201999
	 */
	private static void scrolldown()
	{
		history.setCaretPosition(history.getText().length());
	}
	
	private static String compute()
	{
		// Evaluator e = new Evaluator(textArea.getText());
		// return Double.toString(e.getExpressionValue()); // localization error
		return new DecimalFormat().format(new Evaluator(textArea.getText()).getExpressionValue(), new StringBuffer(), new FieldPosition(0)).toString();
	}
}