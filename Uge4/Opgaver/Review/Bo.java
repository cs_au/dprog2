public class Bo
{
	private static int counter = 0;
	
	public static void main(String[] args)
	{
		/*
		Employee e1 = new Employee("Employee Ernst");
		Employee e2 = new Manager("Manager Moe");
		
		Manager m1 = new Manager("Manager Molly");
		
		System.out.println(e1);
		e1.kastOp();
		System.out.println(e2);
		e2.kastOp();
		System.out.println(m1);
		m1.kastOp();
		*/
		
		/*
		int i1 = 1;
		int i2 = new Integer("H");
		int i3 = '2';
		int i4 = 2 + 'H';
		
		System.out.println(i1);
		System.out.println(i2);
		System.out.println(i3);
		System.out.println(i4);
		*/
		
		/*
		Ib ib1 = new Ib();
		ib1.m(5, 9);
		*/
		
		System.out.println(Exp(1,2));
		System.out.println(counter);
		counter = 0;
		System.out.println(Exp(2,30));
		System.out.println(counter);
		counter = 0;
	}
		
	public static int Exp(int x, int n)
	{	
		if (n == 0)
			return 1;
		else if (n == 1)
			return x;
		else if (n % 2 == 0 && n != 2)
			return Exp(Exp(x,n/2),2);
		else
			return x * Exp(x,n-1);
	}
}