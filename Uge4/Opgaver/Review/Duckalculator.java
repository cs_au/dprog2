import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Duckalculator
{	
	private static JTextArea textArea = new JTextArea(1,1);
	
	public static void main(String[] args)
	{
		ShowGUI();
	}
	
	public static void ShowGUI()
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String keyLabels =  "123+"
						  + "456-"
						  + "789*"
						  + "0/.=" ;//keypad layout 4*4
		JPanel numPanel = new JPanel();
		numPanel.setLayout(new GridLayout(4,4));
		for (int i=0; i<keyLabels.length(); i++)
		{
			final String label = keyLabels.substring(i, i + 1);
			JButton bu = new JButton(label);
			numPanel.add(bu);
			if (i < keyLabels.length()-1) {
				bu.addActionListener(new
					ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							textArea.append(label);
						}
					});
			}
			else {
				bu.addActionListener(new
					ActionListener()
					{
						public void actionPerformed(ActionEvent e)
						{
							textArea.append("\nResult: " + compute()+"\n");
						}
					});
			}
		}
		
		frame.setLayout(new GridLayout(2,1));
		frame.add(textArea);
		frame.add(numPanel);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	private static double compute()
	{
		return 2.0 + 2.0;
	}
}