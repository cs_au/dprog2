public class Manager extends Employee
{
	private String name;
	
	public Manager(String name)
	{
		super(name);
	}
	
	public Manager(String name, boolean bob)
	{
		this.name = name;
	}
	
	public void kastOp()
	{
		try
		{
			throw new FileNotFoundException();
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Sub-class catch - FileNotFoundException");
		}
		catch (IOException e)
		{
			System.out.println("Sub-class catch - IOException");
		}
	}
}