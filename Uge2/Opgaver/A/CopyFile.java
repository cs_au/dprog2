import java.util.*;
import java.io.*;

public class CopyFile
{
	private static String inputFile;
	private static String outputFile;
	private static List<String> outputList;
	
	public static void main(String[] args)
	{
		if (args.length == 2)
		{
			// Assign filename arguments to variables and create a list.
			inputFile = args[0];
			outputFile = args[1];
			outputList = new ArrayList<String>();
			
			try
			{
				// Attempt to load the inputFile for reading.
				BufferedReader r = new BufferedReader(new FileReader(inputFile));
				
				// Extract every line from the file and add it to outputList.
				for(String s = r.readLine(); s != null; s = r.readLine())
				{
					outputList.add(s);
				}
				
				// Close file.
				r.close();


				// Create the outputFile.
				FileWriter w = new FileWriter(outputFile);
				
				// Write all strings from outputList to outputFile.
				for(String s : outputList)
				{
					w.write(s + "\n");
				}
				
				// Close file.
				w.close();
			}
			catch (IOException e)
			{
				System.out.println("Tror da godt nok lige du har lavet noget gal!");
				return;
			}
		}
		else
		{
			System.out.println("Fuck dig, Sverre!");
		}
	}
}