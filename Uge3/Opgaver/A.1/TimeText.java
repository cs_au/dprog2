import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class TimeText
{
	public static void main(String[] args)
	{
		final JTextField tf1 = new JTextField(20);
		
		JButton bu1 = new JButton("Update for glory!");
		bu1.addActionListener(new
			ActionListener()
			{
				public void actionPerformed(ActionEvent event)
				{
					String dateAndTime = new Date().toString();
					tf1.setText(dateAndTime);
				}
			});
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(2,1));
		
		frame.add(tf1);
		frame.add(bu1);
		
		frame.pack();
		frame.setVisible(true);
	}
}