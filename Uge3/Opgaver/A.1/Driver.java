import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Driver
{
	public static void main(String[] args)
	{
		JButton bu1 = new JButton("What time is it?");
		JLabel la1 = new JLabel(new CoffeeMugIcon(128));
		
		bu1.addActionListener(new
			ActionListener(){
				public void actionPerformed(ActionEvent event)
				{
					msg();
				}
			});
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new FlowLayout());
		
		frame.add(bu1);
		frame.add(la1);
		
		//frame.setSize(400, 300);
		frame.pack();
		frame.setVisible(true);
	}
	
	private static void msg()
	{
		JOptionPane.showMessageDialog(
			null,
			"Hello, CoffeeMug!",
			"Coffee Time!",
			JOptionPane.INFORMATION_MESSAGE,
			new CoffeeMugIcon(128));	
	}
}