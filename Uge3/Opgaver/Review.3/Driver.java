public class Driver
{
	public static void main(String[] args)
	{
		C1 c1 = new C1("Bob");
		System.out.println(c1);
		
		I i1 = c1;			// 1 - OK - OK
		System.out.println(i1);
		
		C2 c2 = new C2("Alice");
		System.out.println(c2);
		//I i2 = c2;		// 2 - Error - Compile (incompatible types)
		
		//I i2 = (I)c2;		// 3 - Error - Exception (class cast exception)
		
		//c1 = i1;			// 4 - Error - Compile (incompatible types)
		
		c1 = (C1)i1;		// 5 - OK - OK
		
		//c1 = (C1)c2;		// 6 - Error - Compile (inconvertible types)
	}
}