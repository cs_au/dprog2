public class C1 implements I
{
	private String name;
	
	public C1(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return "Hello, Moshi: " + name;
	}
	
	public void sayHello(String text)
	{
		System.out.println(text);
	}
}