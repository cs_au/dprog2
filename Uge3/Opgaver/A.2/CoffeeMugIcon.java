import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class CoffeeMugIcon implements Icon
{
	private int width;
	
	public CoffeeMugIcon(int width)
	{
		this.width = width;
	}
	
	public void paintIcon(Component c, Graphics g, int x, int y)
	{
		// Mug rectangle and top/bottom ellipses
		int mugBaseX =			x + width/5;
		int mugBaseY =			y + width/4;
		int mugWidth = 			width/2;
		int mugHeight = 		width/2;
		int mugTopBotHeight =	width/8;
		int mugTopBaseY =		mugBaseY - (mugTopBotHeight/2);
		int mugBotBaseY =		mugBaseY + mugHeight - (mugTopBotHeight/2);
		
		// Mug inset/coffee in mug
		int insetBaseX =		mugBaseX + mugWidth/10;
		int insetBaseY =		mugTopBaseY + mugTopBotHeight/5;
		int insetWidth =		mugWidth - mugWidth*2/10;
		int insetHeight =		mugTopBotHeight - mugTopBotHeight*2/5;
		
		// Handle
		int handleWidth =		mugHeight*2/3;
		int handleBaseX =		mugBaseX + mugWidth - handleWidth/2;
		int handleBaseY =		mugBaseY + handleWidth/4;
		int handleInnerWidth =	handleWidth/2;
		int handleInnerX =		handleBaseX + handleWidth/4;
		int handleInnerY =		handleBaseY + handleWidth/4;
		
		Graphics2D g2 = (Graphics2D) g;

		// Top ellipse of the mug		
		Ellipse2D.Double top = new
			Ellipse2D.Double(mugBaseX, mugTopBaseY, mugWidth, mugTopBotHeight);
			
		// Bottom ellipse of the mug
		Ellipse2D.Double bot = new
			Ellipse2D.Double(mugBaseX, mugBotBaseY, mugWidth, mugTopBotHeight);
		
		// Mid-section of mug
		Rectangle2D.Double mid = new
			Rectangle2D.Double(mugBaseX, mugBaseY, mugWidth, mugHeight);
		Line2D.Double midLeft = new
			Line2D.Double(mugBaseX, mugBaseY, mugBaseX, mugBaseY + mugHeight);
		Line2D.Double midRight = new
			Line2D.Double(mugBaseX + mugWidth, mugBaseY, mugBaseX + mugWidth, mugBaseY + mugHeight);
			
		// Mug inset
		Ellipse2D.Double inset = new
			Ellipse2D.Double(insetBaseX, insetBaseY, insetWidth, insetHeight);
		
		// Background
		Ellipse2D.Double back = new
			Ellipse2D.Double(x,y,width,width);
		
		Ellipse2D.Double handleOuter = new
			Ellipse2D.Double(handleBaseX, handleBaseY, handleWidth, handleWidth);
		
		Ellipse2D.Double handleInner = new
			Ellipse2D.Double(handleInnerX, handleInnerY, handleInnerWidth, handleInnerWidth);
		
		g2.setColor(new Color(119,133,186));	// Blue
		g2.fill(back);
		g2.setColor(new Color(212,23,48));		// Red 
		g2.fill(handleOuter);
		g2.setColor(new Color(119,133,186));	// Blue
		g2.fill(handleInner);
		g2.setColor(new Color(212,23,48));		// Red 
		g2.fill(bot);
		g2.fill(mid);
		g2.fill(top);
		g2.setColor(new Color(77,39,21));		// Brownish
		g2.fill(inset);
	}
	
	/**
	 * Returns the width of the icon.
	 */
	public int getIconWidth()
	{
		return width;
	}
	
	/**
	 * Returns the width of the icon, because the icon is
	 * square and thus doesn't need a separate height variable.
	 */
	public int getIconHeight()
	{
		return width;
	}
}