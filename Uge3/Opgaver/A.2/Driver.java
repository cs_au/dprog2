import java.awt.*;
import javax.swing.*;

public class Driver
{
	public static void main(String[] args)
	{
		CompositeIcon ci = new CompositeIcon();
		Icon i1 = new CoffeeMugIcon(128);
		Icon i2 = new CoffeeMugIcon(64);
		Icon i3 = new CoffeeMugIcon(256);
		ci.addIcon(i1);
		ci.addIcon(i2);
		ci.addIcon(i3);
		
		JLabel la1 = new JLabel(ci);
		
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setLayout(new FlowLayout());
		frame.add(la1);
		
		frame.setSize(500,300);
		frame.setVisible(true);
	}
}