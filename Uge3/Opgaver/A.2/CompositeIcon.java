import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.List;
import java.util.*;

public class CompositeIcon implements Icon
{
	private int width, height;
	private List<Integer> wList, hList;
	private List<Icon> iList;
	
	// Constructor
	public CompositeIcon()
	{
		iList = new ArrayList<Icon>();
		wList = new ArrayList<Integer>();
		hList = new ArrayList<Integer>();
		
		width = 0;
		height = 0;
	}
	
	// Add primitive object to composite
	public void addIcon(Icon ic)
	{
		/*
		 * Add the icon to the icon list, and add its width
		 * and height to the width and height lists.
		 */
		iList.add(ic);
		wList.add(ic.getIconWidth());
		hList.add(ic.getIconHeight());
		
		/*
		 * The components of the CompositeIcon are drawn
		 * from left to right.
		 * Add the new icon's width to the width variable,
		 * and update the height of the CompositeIcon, in
		 * case the new icon's height is greater than the
		 * current height.
		 */
		width += ic.getIconWidth();
		updateCompositeHeight();
		
		assert areDimensionsCorrect();
	}
	
	// Calls paintIcon on all icons in the list.
	public void paintIcon(Component c, Graphics g, int x, int y)
	{
		int nextX = x;
		
		for(Icon i : iList)
		{
			i.paintIcon(c, g, nextX, y);
			nextX += i.getIconWidth();
		}
	}
	
	/**
	 * Checks if the dimensions of the CompositeIcon
	 * are correct.
	 */
	private boolean areDimensionsCorrect()
	{
		width = 0;
		
		for(int i : wList)
		{
			width += i;
		}
		
		return width == this.width;
	}
	
	/**
	 * If the CompositeIcon's height is 
	 */
	private void updateCompositeHeight()
	{
		if (height < Collections.max(hList))
			height = Collections.max(hList);
	}
	
	// Required
	public int getIconWidth()
	{
		return width;
	}
	
	// Required
	public int getIconHeight()
	{
		return height;
	}
}